+++
title = "About"
date = "2019-07-14"
+++

## Do You Need Help Writing a Blog?

I have been writing legal and business content for the web since 2000. Currently I ghostwrite for more than a dozen law firm blogs, covering subjects from personal injury and probate to criminal defense and intellectual property. I also contribute to the [Zip2Tax](https://blog.zip2tax.com/author/s-m/) blog, which covers news related to sales and use taxes.

Previously, I spent more than a decade working as a paralegal and writing about antitrust law for a nonprofit organization. If you need an affordable, experienced writer to help create or improve your own blog, [contact me](mailto:contact@skipoliva.com) today. 

## Custom Content for Your Blog

Blogging is an important tool for promoting a business. Unlike newer forms of social media, blogging draws potential customers to your website rather than a third-party platform. A blog also allows you to demonstrate your expertise and accomplishments within your industry.

But while many small businesses and law firms start blogs, they often end up gathering virtual dust after just a few months. It’s not that blogging is difficult, or that your business lacks potential content. It’s just that blogging requires time and effort you would rather devote to serving your existing customers.

This is where I can help. I’ve blogged professionally since 2000–before the term “blog” even entered the popular lexicon. I’ve served a number of professional clients, including [Thomson Reuters' SuperLawyers](https://www.superlawyers.com/articles/writer/sm-oliva/6f81a3f2035d3031ae59ced967fe5509.html) and [Lawrence Technology Services](https://www.lawrencesystems.com/the-kaseya-malware-attack-when-is-a-company-legally-liable-for-a-data-breach/). I know how to research and write timely content that will ensure your blog never goes stale.

## How Blog Ghostwriting Works

Every business and blog setup is unique. I have some clients who prefer to update their blogs 2 or 3 times per week, and others who only do so once or twice a month. The right blogging schedule depends on a number of factors, including the types of services you offer, how much news there is about your subject, and your budget for ghostwriting services.

Whatever your needs, [contact me](mailto:contact@skipoliva.com) today so we can develop a custom ghostwriting package for your business. As far as the final content is concerned, you will retain full ownership and copyright over all paid work-for-hire.

## Disclaimer

This website is provided for informational purposes only. The author is not an attorney and neither he nor S.M. Oliva, LLC, offers anything constituting legal advice. Always consult with a licensed attorney before relying on any information you obtain from this or any online source. 
