---
author: "S.M. Oliva"
date: 2019-01-24
title: Server Shenanigans Cost California Business Owner Over $2.4 Million
tags: [California, discovery sanctions, R Consulting, Info Tech Corporation, expert witness]
categories:
    - Contract Law
---

In any civil lawsuit, both sides have a legal obligation to preserve any data or documents that may be relevant to the case. If a party intentionally withholds or destroys evidence that is subject to a legitimate discovery request, the trial judge may impose sanctions–including dismissing the case (if the plaintiff was at fault) or granting default judgment (if the defendant was at fault).

## *R Consulting & Sales, Inc. v. Info Tech Corporation*

A recent unpublished decision from the California Fourth District Court of Appeal, [*R Consulting & Sales, Inc. v. Info Tech Corporation*](https://scholar.google.com/scholar_case?case=17658415056457773780), offers a stark example of the latter scenario. In this case, a San Diego judge issued a $2.4 million default judgment against a corporate defendant and its owner after the plaintiff presented credible evidence of intentional destruction of an email server. The appellate court declined to reverse the trial judge’s decision, holding that defendants’ actions justified the severe sanction.

Here is a brief overview of what happened. The plaintiff leased a private jet to the defendants. When the defendants failed to make the required payments, the plaintiff file a lawsuit in California Superior Court alleging breach of contract. The lawsuit further alleged the defendants failed to pay various vendors they contracted to perform work on the leased jet–which meant the plaintiff could be on the hook for those bills as well.

Approximately six months after the plaintiff filed its lawsuit, the defendant company apparently moved its servers to a third-party data center. The following year, the plaintiff requested a number of “missing emails” that were not produced in response to earlier discovery requests. The defense responded that due to the relocation of the servers, “some of the documents” requested had been deleted but that it might be possible to recover them from retired servers kept in storage. 

The defendant owner–who was not a IT expert himself–then filed a declaration with the court, in which he insisted it would take “200,000 hours, the equivalent of 25,000 eight-hour work days,” to actually restore the requested files. A digital forensics expert retained by the plaintiff called this estimate “absurd.” He wanted to physically inspect the servers in order to provide the court with a more credible cost estimate, but the defense “refused to provide their location.”

After some back-and-forth with the judge, the court ordered the defendant owner to “produce the documents at his own expense” and imposed sanctions of $16,300.

## Expert Witness: Defense “Sabotaged” Email Servers

Several months passed. Under the threat of additional sanctions, the defense finally allowed another digital forensics expert retained by the plaintiff to inspect the email server. Here is a rundown of what the expert found:

* The email server contained two physical hard drives and was connected to a storage array with 14 additional hard drives.
* One of those 14 drives was “not fully connected” to the storage array.
* The expert could not retrieve any email files from the server or the array.
* The two hard drives purportedly from the email server did not have an operating system, which indicated they were “not the original hard drives” for that server.
* Based on “email fragments” recovered from some of the drives, the expert determined that the two server drives “actually came from the storage array” and that someone had deliberately moved the drives from the array to the server.
* “Shuffling” the drives around “destroyed” the logical volumes in the storage array, making recovery of the data impossible.
* The shuffling took place on or after the date the plaintiff initially sought discovery of the emails.

The defense also retained a digital forensics expert. The defense expert did not personally examine the servers, however, but instead relied on interviews he conducted with the defendant owner. From this, the defense expert said there was “a wide range of possible” explanations for why the requested data could not be recovered from the server, and and that it could not “be said to a reasonable degree of certainty that the server was ‘sabotaged’ by anyone prior to the inspection” by the plaintiff’s expert.

In rebuttal, the plaintiff’s expert submitted that it was not industry practice for a digital forensics expert to “simply review another expert’s report and refute it based on only what is contained within a declaration,” rather than conducting an independent physical examination of the server himself.

The trial judge similarly concluded the defense expert’s conclusions were nothing more than “speculation and conjecture.” More to the point, they did not refute the “persuasive” findings of the plaintiff’s expert–namely that the defense deliberately sabotaged the server in order to avoid complying with the plaintiff’s discovery requests. On that basis, the judge imposed “terminating sanctions” against the defendants. That is to say, the court granted default judgment to the plaintiff and ordered the defendants to pay damages in excess of $2.4 million.

## Appeals Court: Sanctions Do Not Require Evidentiary Hearing

The defendants appealed to the Fourth District. The main thrust of the appeal was the defense’s belief that the trial judge improperly accepted on the “contradicted hearsay” evidence offered by the plaintiff’s expert. Keep in mind, the judge did not conduct a full evidentiary hearing. Rather, he relied on affidavits filed by the experts. The defense maintained they were entitled to cross-examine the plaintiff’s expert at an in-person hearing before the judge issued a final decision.

The Fourth District disagreed. The judge was not trying the merits of the plaintiff’s case. Rather, he ruled on a motion for sanctions filed by the plaintiff. When deciding such motion, a judge is permitted to rely solely on affidavits. California law “does not require an evidentiary hearing,” the appeals court observed, “only notice and an opportunity to be heard, which defendants received.” Furthermore, the defendants never requested a live hearing prior to the judge’s decision–they actually waited until a month later to raise the issue of cross-examination. 

As for the decision itself, the Fourth District said it was supported by “substantial evidence.” As noted above, the defense expert never actually refuted the conclusions made by the plaintiff’s expert. And the defense never challenged the underlying premise of the plaintiff’s motion–that it was “prejudiced by the loss of the data on the servers.” So the $2.4 million default judgment stood.