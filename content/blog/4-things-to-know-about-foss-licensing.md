---
title: "4 Things to Know About FOSS Licensing"
date: 2019-01-29
author: "S.M. Oliva"
categories: "FOSS"
tags: [copyright, Linux, open source, GPL, BSD, Apache license, Ubuntu]
---

FOSS stands for *free and open source software*. This refers to a method of distributing software rather than its price. In fact, there are many companies that make money supporting FOSS. 

So what actually makes software "free and open source"? The [Open Source Initiative](https://opensource.org/) (OSI), a California-based nonprofit organization that serves as the *de facto* standards body for FOSS, has a [10-point list of criteria](https://opensource.org/osd). Essentially, FOSS is software that can be freely redistributed and includes not just the executable program--which typically exists in machine-readable binary form--but also the human-readable version or *source code*.

There are actually a number of different approaches to distributing FOSS. OSI maintains a [list of licenses and standards](https://opensource.org/licenses) commonly used by FOSS developers. This article will not go into extensive detail about the different licenses and their applications. Instead, the focus here will be on identifying and addressing certain misconceptions you might have about FOSS.*

## 1. FOSS Is Not Public Domain.

Generally speaking, you can use, copy, and redistribute FOSS programs without restriction. So unlike, say, Microsoft Windows, you do not need to pay a separate licensing fee for every machine you install a FOSS program on. Given this "freedom," you might think that FOSS qualifies as public domain software.

But that is not how copyright works. According to the [United States Copyright Office](https://www.copyright.gov/help/faq/faq-general.html#protect), copyright automatically protects any "original works of computer software" from the moment "it is created and fixed in a tangible form that it is perceptible either directly or with the aid of a machine or device." For most works created since 1978--which is just about all modern computer software--the copyright term lasts until 70 years after the death of the original author. If the work was made for-hire (such as for a company) or the author is unknown, the copyright lasts until the earlier of 95 years from the year of first publication or 120 years from the year of creation. Again, this pretty much covers all computer software.

The *public domain* refers to works that either (a) do not meet the legal requirements for copyright protection or (b) are no longer under copyright due to the expiration of the applicable term, as described above. So even if a copyrighted computer program is made available under a FOSS license, it is still not in the "public domain" until such time as the copyright has expired.

## 2. FOSS Is Not "Freeware".

When I started using computers in the late 1980s, you often heard the term *freeware* used to describe software that was made available to users at no charge. You don't hear this term used much anymore, although the concept of freeware still exists. Many popular business applications, such as Skype and Google Chrome, are essentially freeware.

So how does freeware differ from FOSS? Well, as noted above, a FOSS license requires the disclosure of source code. In contrast, commercial freeware typically includes just the machine-readable executable code. This means you can run the program for free, but you cannot actually look at (or alter) the original code written by the developers.

In the old days of Linux--i.e., the mid-to-late 1990s--you would often see distributions come with two sets of installation disks or DVDs. One set would contain the executable files to run on your computer. The other would contain the same files in their source code form. Today, Linux distributions simply make the source code versions available online to anyone who wants to look at them.

## 3. "Free Software" Is Not a Legal Term.

There are two basic types of FOSS licenses. The first is a *reciprocal* license. This means that the copyright holder allows anyone to modify and redistribute the licensed program, provided they do so under the same terms as the original license. The most commonly used reciprocal license is the [GNU General Public License](https://opensource.org/licenses/gpl-license) (GPL). As Judge Frank Eaterbrook of the U.S. Seventh Circuit Court of Appeals described the GPL in a [2006 decision](https://scholar.google.com/scholar_case?case=3689055814945636124):

>The GPL imposes an affirmative obligation on any license holder to make the code of any derivative work freely available and open source. If the license holder fails to comply ... the GPL purports to terminate, and the license holder is potentially liable for copyright infringement for distributing or copying the software without permission.

Proponents of the GPL often describe their approach to licensing as "free" software. But this is a political term without legal significance. (You may also hear the GPL referred to as a "copyleft" license, which again is a political term of art.) In fact, reciprocal licenses such as the GPL are actually more restrictive than the second major type of FOSS license--which, you probably guessed, is a *non-reciprocal* license.

A non-reciprocal license basically says, "Here's some software.; do whatever you want with it." Unlike the GPL, there is no affirmative obligation on the part of the authors of any derivative works to license their own work under the same license. For example, Apple originally built its popular macOS operating system using source code from the Berkeley Software Distribution (BSD) UNIX project. The [BSD license](https://opensource.org/licenses/BSD-3-Clause) is non-reciprocal, so Apple did not have to license its macOS modifications under the same terms as BSD. 

## 4. FOSS Licenses Do Not Cover Trademarks.

As Judge Easterbrook noted, a FOSS license is designed to protect the licensee from copyright infringement claims arising from the use or redistribution of the software. Many FOSS licenses, such as the GPL and the non-reciprocal [Apache License](https://opensource.org/licenses/Apache-2.0), also protect against potential patent infringement claims. But one thing FOSS licenses typically do not cover are the right to use trademarks or service marks.

In other words, just because you are free to modify and redistribute someone else's software, that doesn't mean you can use their branding. For instance, here is the [official policy of Canonical](https://www.ubuntu.com/legal/intellectual-property-policy), the publisher of the Ubuntu Linux distribution, with respect to the use of its registered marks:

>Any redistribution of modified versions of Ubuntu must be approved, certified or provided by Canonical if you are going to associate it with [our] Trademarks. Otherwise you must remove and replace the Trademarks and will need to recompile the source code to create your own binaries. This does not affect your rights under any open source licence applicable to any of the components of Ubuntu.

And while FOSS licenses do not allow the original author to charge royalties in connection with the software itself, publishers like Canonical may collect such fees for the use of their marks. 