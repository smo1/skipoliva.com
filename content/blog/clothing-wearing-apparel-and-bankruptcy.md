---
title: "Clothing, Wearing Apparel, and Bankruptcy"
date: 2019-12-01
author: "S.M. Oliva"
categories: 
    - Bankruptcy
tags: [Wyoming, Virginia, wearing apparel, Chapter 7]

---

In bankruptcy a debtor is allowed to keep a certain amount of clothing or "wearing apparel." Under [federal bankruptcy rules](https://www.law.cornell.edu/uscode/text/11/522#), wearing apparel is considered part of a single exemption that also covers household furnishings, appliances, books, and other items that are "held primarily" for personal or family use. For bankruptcy petitions filed after April 1, 2019, the total federal exemption under this category is $13,400, although any individual item is only exempt up to $625.

Some states treat clothing or wearing apparel as a separate exemption. In [Virginia](https://law.lis.virginia.gov/vacode/title34/chapter3/section34-26/), for example, "all wearing apparel" of a member of the debtor's household is exempt up to $1,000. Unlike the federal exemption, there is no per-item limit. 

## When Are Wedding or Engagement Rings Considered "Wearing Apparel"?

One issue that occasionally comes up in bankruptcy cases is whether or not certain items of jewelry, notably wedding rings, can be considered "wearing apparel" for purposes of the exemption. This is not an issue in Virginia, where "wedding and engagement rings" have their own exemption. Indeed, Virginia's exemption does not have a limit, which suggests that a debtor could exempt an expensive wedding ring without regard to its market value.

In other states, wedding rings are expressly included in the definition of wearing apparel. [Wyoming](https://law.justia.com/codes/wyoming/2013/title-1/chapter-20/section-1-20-105/) law currently exempts up to $2,000 in "necessary wearing apparel," which excludes all types of jewelry "other than wedding rings." 

Now, a question that might not have occurred to you is, "Does it have to be *my* wedding ring in order to qualify for the exemption?" The Wyoming Supreme Court actually addressed this question in a 2002 decision, [*In re Winters*](https://scholar.google.com/scholar_case?case=373639743299349012). In that case, a debtor filed for Chapter 7 bankruptcy and claimed Wyoming's exemption for two rings that the debtor had inherited. They had previously been the wedding and engagement rings of the debtor's mother. The debtor herself, however, was unmarried. Nevertheless, she claimed the rings were wearing apparel, as she wore the engagement ring on a "regular basis" and occasionally wore the wedding ring for "sentimental reasons."

The bankruptcy judge, unsure of how to proceed, asked the Wyoming Supreme Court to clarify whether the debtor's rings qualified as "necessary wearing apparel" under the state exemption. The Supreme Court replied they did not. Although state law did not expressly define the terms "necessary wearing apparel" or "wedding rings," the Court held that the legislature's decision to exclude "jewelry of any type" from the exemption with only a limited exception for "wedding rings" indicated the terms should be defined narrowly. And it was more in keeping with "basic bankruptcy principles" to permit debtors "an exemption as to their own personal wedding rings" only.

To rule otherwise--that "all wedding rings, whatever their nature," were exempt as necessary wearing apparel--would, in the Court's view, stretch the scope of the exemption "to the point of absurdity."

## Jewelry and Wildcard Exemptions May Be Available

It is worth noting that Wyoming did not--and as far as I know, still does not--have specific jewelry or "wildcard" exemptions for debtors. Such exemptions often enable a debtor to cover items such as wedding or engagement rings even when they do not fall under a wearing apparel exemption. For instance, the federal rules do not expressly cover wedding rings, but they do provide a $1,700 exemption for jewelry. There is also a federal wildcard exemption equal to $1,325 plus any unused portion of the federal homestead exemption (i.e., the value of the debtor's primary residence) up to $12,575.

