---
title: "3 Key Differences Between Managing Files in Linux and Windows"
date: 2019-01-08
author: "S.M. Oliva"
categories: "FOSS"
tags: [Linux, Ubuntu, Stardew Valley]
---


At first glance, a Linux-based operating system like [Ubuntu MATE](http://ubuntu-mate.org/) does not look all that different from Windows, especially if you are currently using older versions like Windows 7 (or even Windows XP). But as you start to work with Linux, you'll notice some subtle variations in how the system deals with files and disk drives. 

Here are three things to keep an eye out for:

## 1. There Is No C:\ Drive.

Since time immemorial, as Tony Soprano used to say, each disk drive on a Windows system has its own special letter assigned to it. The main hard drive is always the ```C:\ ``` drive. My Windows 10 PC assigns ```D:\ ``` to the recovery partition and ```F:\ ``` to the first USB drive I plug in. And of course, back in the halcyon days of floppy disks, those drives were usually assigned```A:\ ``` or ```B:\ ```. 

But Linux doesn't assign drive letters. In fact, Linux doesn't recognize disk drives as separate entities. In Linux, everything is connected to what is called the *root* filesystem, which is simply designated as ```/ ```. (Also note that Linux uses a forward slash, as opposed to the backward slash in Windows.) No matter how many separate disk drives you attach to a given computer, they are all organized as directories and sub-directories under root.

Pretty much every Linux-based operating system--at least the ones I've used--rely on some variation of the [Filesystem Hierarchy Standard](https://wiki.linuxfoundation.org/lsb/fhs-30) (FHS) to organize the directories under root. For instance, here is what the root directory looks like on my Lenovo ThinkPad running Ubuntu MATE:

![Image 1](/blog/images/2019-01-08-file-system.png)

It's not important to know what most of the directories contain. (I don't.) The critical directory for our purposes is ```/home ```, which as the name suggests serves as the user's home folder. In this sense, ```/home ``` is the equivalent of ```C:\Users ``` on Windows 10. Each user on a system has a separate sub-directory within ```/home ```, such as ```/home/smoliva ``` in my case.  By default many Linux systems, including Ubuntu MATE, also create a number of sub-directories in each user's home folder, including ```Documents ```, ```Downloads ```, ```Music ```, ```Pictures ```, ```Templates ```, and ```Videos ```. 

Now, with respect to disk drives, an FHS-compliant filesystem classifies these as *device* files, which are located in the ```/dev ``` directory under root. Individual hard drives are typically designated as ```/dev/sdax ```, with *x* representing a drive number as opposed to a letter. For example, I currently have two solid state drives attached to my ThinkPad. The first drive is ```/dev/sda1 ```, and the second is ```/dev/sda2 ```. And if I plug in a USB drive, it will usually show up as ```/dev/sdb1 ```.

If that sounds too confusing, don't worry. To simplify things for the user, Ubuntu-based systems usually mount (attach) any additional disk drives to another directory called ```/media ```. The drive can then be accessed at ```/media/username/drivename ```. But it's not even necessary to remember this path, because the MATE desktop automatically displays an icon shortcut to any attached drive directly on the user's desktop. 

For example, let's say a user with the username "msimpson" has a second hard disk named "Backup" and a USB thumb drive called "SolusLiveBudgie" attached to her laptop. The MATE desktop would display the following icons: 

![Image 2](/blog/images/2019-01-08-disk-icons.png)

## 2. There Are No .exe Files.

In recent years, Windows has tried to move users towards a centralized store-based model for installing new programs, similar to how iPhone and Android phones work. Linux actually pioneered this approach with its *repository* system. Each Linux distribution, such as Ubuntu, maintains repositories containing software that has been tested and verified to work with the rest of the operating system. 

But there are times when users need--or want--to install software that is not available through a repository. In Windows, you can famously download and run any file with an ```.exe `` extension. So what is the Linux equivalent of this?

The answer is somewhat complicated. There are multiple ways to install software on a Linux system outside of the official repositories. I will go into more detail about this in a future post. But for now, I'll highlight the one method that is closest to "download-and-install an exe file." 

Many commercial programs sold for Linux come in the form of a single file with a ```.sh ``` extension. This stands for "SHell script," and it refers to [Bash](https://www.gnu.org/software/bash/), which is a basic programming language recognized by most Linux-based operating systems. A Bash script can be run in much the same way as a Windows ```.exe ``` file.

However, if you simply download a ```.sh ``` file and double-click it on your Linux desktop, it's likely nothing will happen. The reason for this is that you need to first designate the file as "executable" by the system. In the MATE desktop, you can do this by right-clicking on the filename, selecting ```Properties ``` from the menu that appears, and checking a box that says, appropriately enough, "Allow executing file as program." Below is an illustration from my system, using the installer for the popular game [Stardew Valley](https://www.stardewvalley.net/) that I purchased from from [GOG.com](https://www.gog.com/).

![Image 3](/blog/images/2019-01-08-permissions.png)

## 3. You Need to Enter Your Password to Make Changes to the System.

So after you double-click your newly-executable shell script, another dialog box may appear asking you to enter your password to continue. If you're used to the Windows approach to installing software--where you're asked to click a box but not enter a password--this might seem like an unnecessary step. After all, if you're already logged in to your computer, why would you need to enter your password a second time?

This is actually a critical component of how Linux secures a computer. Linux was modeled after other multi-user computer systems from back in the day when mainframes were all the rage. And even today, when most PCs only have one user, there is still a sharp distinction between the *user* of a system and the *administrator*.

This goes back to the root filesystem discussed above. There is, in fact, a separate ```root ``` user as well, which has its own home folder located at ```/root ```. Ubuntu-based desktop Linux systems do not allow users to access the root user by default. But in order to make changes to the system--i.e., installing or removing programs--the user still needs to have access to root-level administrative privileges.

This is where handy little program called [sudo](https://www.sudo.ws/) comes in. Sudo enables the user to "borrow" the root user's ability to make changes to the system for a short time. So when the dialog box asks you to re-enter your user password, it is actually seeking authorization to run```sudo ``` , i.e. carry out a command as if you were the root user.

One caveat to the sudo rule is worth pointing out here: If you install a program to your home folder, sudo is not necessary provided you don't make changes at the root level. So if I double-clicked the Stardew Valley installation program, for instance, and set the game to install at```/home/smoliva/stardew-valley ```, I would not need to re-enter my password. But if I wanted to install the game in ```/bin ```, which is where many of the system-level programs are stored, then I would need to enter a password to gain sudo-level permissions.