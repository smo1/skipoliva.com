---
title: "ABB, Inc., Faces Lawsuit Over Health Plan Data Breach"
date: 2019-05-30
author: "S.M. Oliva"
tags: [ABB, North Carolina]
categories:
    - Data Breach Litigation

---

On May 22, two Arkansas residents filed a [class action complaint](https://www.courtlistener.com/recap/gov.uscourts.nced.171601/gov.uscourts.nced.171601.1.0_2.pdf) against ABB, Inc., a North Carolina-based industrial technologies company, seeking damages on behalf of approximately 18,000 people allegedly affected by a [September 2017 data breach](https://www.courtlistener.com/recap/gov.uscourts.nced.171601/gov.uscourts.nced.171601.1.1.pdf) involving ABB's health plan. The plaintiffs, an employee of an ABB subsidiary and his spouse, claim the company "failed to comply with security standards and allowed" their personal identifying information to be acquired by unknown attackers. 

In a notice to employees dated September 7, 2017, ABB disclosed to its employees that it learned two weeks prior of a "data security incident" whereby a "few email accounts had suspicious login activity as the result of a phishing scheme." These compromised accounts "may have stored" personal identifying information of ABB employees, including their names, addresses, birth dates, Social Security numbers, and salary information. 

The plaintiffs' class action complaint further explained the data breach specifically targeted information used by ABB's health benefits plan. This means the data breach affected not only the personal information of ABB employees, but also their spouses and other family members who participate in the plan. Altogether, the complaint alleges there are "approximately 17,996 active participants in the plan as of the end of 2016."

The complaint states that because of the data breach, the named plaintiffs and other affected individuals "have suffered damages by taking measures to deter, detect, and stop identity theft." The named plaintiff, who works for an ABB subsidiary based in Arkansas, said he "stopped making" contributions to his 401(k) plan "specifically in response to the breach for fear that his information would be further compromised." Consequently, the plaintiff has been "taxed on income that would have [otherwise] invested tax-deferred."

Meanwhile, the spouse plaintiff said she was the target of "five unauthorized credit card inquiries with banking institutions in four different states, indicating identify theft." This forced both named plaintiffs to spend "significant time" to dispute the unauthorized activity. And while the complaint says ABB offered all employees affected by the data breach "at least one year of credit monitoring," the company said nothing with respect to assisting affected spouses or family members. Nor has ABB offered any of the affected individuals "assistance in dealing with the IRS or state tax agencies."

The lawsuit seeks class certification based on multiple claims for relief, including the following:

+ "unfair or fraudulent business practices" under North Carolina law;
+ breach of ABB's fiduciary duty to "properly monitor and safeguard" the class members' personal information;
+ negligence, i.e., failing to exercise "reasonable care" in safeguarding the class members' personal information;
+ violation of the Federal Trade Commission Act, a federal statute governing "unfair" business practices in interstate commerce; and
+ breach of express and implied contract.

As of May 30, 2019, ABB has yet to formally respond to the class action complaint, which was filed in the U.S. District Court for the Eastern District of North Carolina.