---
title: "Allconnect Moves to Settle Employee Data Breach Lawsuit Following Early Loss in Kentucky Court"
date: 2019-05-14
author: "S.M. Oliva"
tags: [Allconnect, Kentucky]
categories:
    - Data Breach Litigation

---

[Allconnect](https://www.allconnect.com/) is a free online service that helps consumers compare prices for various other services, including internet, television, and home security. But as it turned out, Allconnect did not do a great job with respect to its own corporate security. On Valentine's Day 2018, someone impersonating Allconnect's president sent a false email to one of the company's employees. The email requested the previous year's W-2 information for all Allconnect employees. 

The unsuspecting Allconnect employee provided the requested information to the unknown spoofer, which included the names, addresses, and Social Security numbers for all company employees. Allconnect did not discover this security breach for nearly six weeks. And it did not disclose the breach to the affected employees until April 2, 2018, some 47 days after the fact.

A group of Allconnect employees subsequently filed a lawsuit in Kentucky state court, seeking certification as a class action. Allconnect had the case removed (transferred) to a federal court in Lexington, Kentucky. Allconnect then moved to dismiss the plaintiffs' complaint on various grounds.

On March 28, 2019, [U.S. Senior District Judge Joseph M. Hood largely rejected the motion to dismiss](https://scholar.google.com/scholar_case?case=98728043991865712). The crux of Allconnect's argument could be boiled down to three points:

1. The plaintiffs' lacked legal "standing" to bring their lawsuit.
2. The plaintiffs' complaint failed to provide enough information to meet federal pleading standards.
3. The plaintiffs did not meet the requirements for certifying a class action.

## Judge: Employees Have Standing Based on "Lost Control" of Personal Information

Regarding the first issue--standing--Allconnect maintained there was no evidence that the employees affected by the data breach had suffered a concrete legal "injury." Rather, the alleged injury was purely speculative. That is to say, the people who illegally obtained the W-2 information *might* use it for some nefarious purpose in the future. But the employee-plaintiffs could not establish standing based solely on their fear of such future events.

But as Judge Hood explained, many federal courts have concluded in similar cases that the "substantial risk of harm" associated with a data breach, along with the "reasonably incurred mitigation costs," are enough to establish a legal injury, at least for purposes of surviving a motion to dismiss.

Indeed, Judge Hood noted the U.S. Sixth Circuit Court of Appeals--which has appellate jurisdiction over federal courts in Kentucky--reached this exact conclusion in a 2016 decision. In that case, [*Galaria v. Nationwide Mutual Insurance Company*](https://scholar.google.com/scholar_case?case=9209178760290219480), two customers sued their insurance company after a data breach exposed their personal information to attackers. The majority of a three-judge panel said when victims of a data breach "already know that they have lost control of their data, it would be unreasonable to expect [them] to wait for actual misuse--a fraudulent charge on a credit card, for example--before taking steps to ensure their own personal and financial security."

Now, the Sixth Circuit's *Galaria* decision was "unpublished," meaning it is not considered binding precedent. Nevertheless, Judge Hood said the majority's reasoning was "highly persuasive." And like that previous case, the Allconnect employees who filed suit here have "provided factual information that demonstrates that they have lost time and money as a result of taking steps to protect their personal data and prevent the misuse of that data by scammers." This was enough to justify their standing to sue.

### Negligence May Include Employer's Failure to Secure Employee Data

With respect to the specific allegations raised in the plaintiffs' complaint, Allconnect argued it could not be held responsible for the criminal acts of third parties acting without its consent. In response, the plaintiffs alleged Allconnect was negligent in failing to take "reasonable steps to safeguard their personal information."

Judge Hood agreed with the plaintiffs that such a duty existed as part of Allconnects's overall responsibility to "prevent foreseeable harm to its employees." The judge therefore declined to dismiss the plaintiffs' allegations of negligence against Allconnect. Similarly, the judge said the employees could also proceed with a claim for "breach of implied contract."

But Judge Hood dismissed other claims raised by the plaintiffs, including invasion of privacy and breach of fiduciary duty. The judge also reserved judgment on the question of class certification pending further discovery between the parties.

## Lawsuit Stayed Pending Final Settlement Talks

However, it appears the class certification question may now be moot. About a month after Judge Hood issued his decision, the parties informed the Court they were finalizing a settlement. Judge Hood therefore [issued a stay](https://www.courtlistener.com/recap/gov.uscourts.kyed.86134/gov.uscourts.kyed.86134.34.0.pdf), temporarily suspending the litigation. He directed both sides to report back to the Court no later than August 26, 2019. 

