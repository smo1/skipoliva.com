---
title: "Missouri Lawsuit Over Children's Mercy Hospital Data Breach Proceeds"
date: 2019-05-29
author: "S.M. Oliva"
tags: [Children's Mercy Hospital, Missouri]
categories:
    - Data Breach Litigation

---

Not all data breaches are the result of an outside attack. In many cases it is an employee of the company who is responsible for the breach. For example, in March 2017 an employee of Chidlren's Mercy Hospital in Kansas City, Missouri, created an unauthorized website that included confidential patient information, including names, birthdays, phone numbers, medical records, and even diagnoses. And this was not the first time Children's Mercy experienced such a breach--a similar incident occurred in 2015.

Both data breaches prompted litigation. With respect to the 2017 breach, the mother of a minor child affected by the breach filed a [class action petition](https://ecf.mowd.uscourts.gov/doc1/10916833747) in Missouri state court against Children's Mercy in May 2018. Children's Mercy removed the mother's petition to federal court. A jury trial is tentatively scheduled to begin in January 2020.

## No Proof of a Valid Contract

In an effort to avoid a jury, Children's Mercy filed a motion for "judgment on the pleadings" last September, effectively a motion to dismiss the case. In a May 16, 2019, opinion, U.S. District Judge Roseann A. Ketchmark [denied the motion](https://www.courtlistener.com/recap/gov.uscourts.mowd.140562/gov.uscourts.mowd.140562.1.2.pdf) except as to one of the claims raised in the initial petition--an allegation that Children's Mercy's failure to prevent the data breach constituted a "breach of contract."

The alleged contract is actually Children's Mercy's Notice of Health Information Practices, which it posted on the hospital's website. Among other provisions, the Notice states the hospital is "required" to "keep your child's health information private." The plaintiff maintains this "constitutes an agreement between [the hospital] and its patients." 

But Judge Ketchmark said the mere publication of the Notice did not demonstrate the existence of a legally binding contract. The plaintiff's petition did not plead any facts "to show offer, acceptance, or consideration." And there was no legal authority, at least in Missouri, that holds merely submitting private data to a health care provider automatically creates a contract with that provider. For these reasons, the judge dismissed the plaintiff's claim for breach of contract, although she allowed the plaintiff 30 days to revise and refile it. 

## Does the Plaintiff Have Article III Standing?

Unlike many data breach lawsuits, the hospital here did not challenge the plaintiff's Article III standing to sue. Judge Ketchmark addressed the subject anyway, as she explained the court had an "independent obligation to examine its own jurisdiction," which includes standing. Ultimately, the judge concluded the planitiff had standing, even though her specific breach of contract claim was dismissed due to insufficient factual allegations.

Indeed, Judge Ketchmark explained that "facts establishing the legal conclusion of a valid, enforceable contract are not required to assert standing in a breach-of-contract claim." What is required is an allegation of a specific injury. Here, the petition alleged that as a result of the defendant's failure to "implement security measures" to protect patient information, the plaintiff suffered an injury in the form of the "diminished value of her bargain" with the hospital.

## State Tort Claims and "Economic Losses"

For similar reasons, the judge denied Children's Mercy's motion to dismiss the other parts of the plaintiff's lawsuit. For instance, the plaintiff also alleged the hospital violated Missouri's Merchandising Practices Act (MPPA). Essentially, this statute makes it illegal for a business to engage in "deceptive acts, practices, and conduct," which in this case would be the hospital's misrepresentation that it would keep patient data confidential, as well as its failure to actually keep the data safe.

The hospital maintains the plaintiff could not demonstrate an actual loss under the MPPA. Judge Ketchmark disagreed. She said that Missouri courts "apply the benefit-of-the-bargain rule" to MPPA claims. That is to say, a plaintiff may seek the "difference between the actual value of the property and what its value would have been if it had been as represented." In this case, the plaintiff "has plausubly alleged an ascertainable loss."

The plaintiff's other claims against Mercy Children's Hospital are all based in common law torts, such as negligence. Here, Missouri law would normally prevent the plaintiff for recovering damages based on economic losses rooted in a breach of contract. But as discussed above, the judge held there was no valid breach of contract claim currently before the court. And in any event, the plaintiff's tort allegations extend to a variety of non-economic damages, including "embarrassment, humiliation, and loss of enjoyment of life."