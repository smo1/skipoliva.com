---
title: "Using Solus MATE in 2020"
date: 2020-01-01
author: "S.M. Oliva"
tags: 
    - Solus
    - Linux
    - MATE Desktop
    - hledger
categories:
    - FOSS

---

Since 2012, I have used Linux as my day-to-day computer operating system. There are many forms of Linux, which are commonly known as *distributions*. Over the past 8-plus years, I have tried a number of distributions, with varying degreees of success. But moving forward into 2020, my plan is to use a relatively small independent distribution called [Solus](https://getsol.us).

## What Is Solus?

First, let's define what I mean by an "independent" distribution. Most Linux distributions rely on another project for their base of software packages. For example, the popular [Linux Mint](https://linuxmint.com) distribution is based on the package system provided by [Ubuntu](https://ubuntu.com). And Ubuntu, in turn, is based on the [Debian](https://www.debian.org/) packaging system.

Solus, in contrast, is built entirely from the ground-up by its development team. There a number of benefits to this approach if you are a desktop (or laptop) user of Linux:

+ Solus is designed and built exclusively for the x86_64 architecture, which is the common standard used on most modern desktop and laptop computers, including those machines that currently run Windows 10 or macOS.
+ Solus relies on a traditional Linux packaging model, where software can easily be installed from a built-in repository system, similar to an app store.
+ Solus typically provides weekly updates to its software repository, meaning users receive the latest (or nearly latest) versions of applications.

All that said, there are also some limitations you need to be aware of if you are considering transitioning to Solus:

+ Solus uses a packaging format known as *eopkg*, which is not used by any other Linux distribution to my knowledge; this means that packages designed for distributions like Ubuntu cannot be installed directly in Solus.
+ The overall number of applications available in Solus is limited relative to other Linux distributions, and there is no means for users or third parties to maintain outside repositories that integrate with the eopkg system, such as Ubuntu's [personal package archives](https://launchpad.net/ubuntu/+ppas) or the [Arch User Repository](https://aur.archlinux.org/).  
+ Solus does not support certain common Linux desktop environments, including [Xfce](https://xfce.org/), [LxQT](https://lxqt.org/), and Cinnamon. 

## The MATE Desktop

One desktop that Solus does support is [MATE](https://mate-desktop.org/). Although Budgie is the in-house desktop environment provided by the development team, MATE is fully integrated with Solus. This was a key reason I moved to Solus in the first place, as I had previously used [Ubuntu MATE](https://ubuntu-mate.org/) as my pricipal Linux distribution.

By default, the Solus implementation of MATE strongly resembles a stock Windows desktop configuration. There is a single taskbar at the bottom of the screen with a menu (i.e., start) button. The menu, known as Brisk Menu, was actually developed jointly by the Solus and MATE teams. It provides a search function for installed applications, but not files or websites.

![Solus MATE default desktop](/blog/images/2020-01-01-solus-mate-default-desktop.png)

I modified my MATE desktop from the stock configuration to more closely follow the Ubuntu MATE layout:

![Oliva - Modified Solus MATE desktop](/blog/images/2020-01-01-solus-mate-desktop.png)

The main features of this modified layout are:

+ The menu and main taskbar are now on the top of the screen.
+ I moved the time and date display from the right side of the main taskbar to the center.
+ I added a secondary taskbar on the bottom of the screen, which contains links to open windows and a multi-workspace switcher.

The ability to use multiple workspaces (or "virtual desktops") has always been one of my favorite features in Linux, and MATE in particular. Windows 10 actually supports a similar feature, although I find it is not quite as intuitive. MATE provides four workspaces by default, although this can be altered. In my own daily work, I typically use three workspaces, one for my active writing project, a second for my schedule, and a third for other applications.

## Applications

So what applications do I use? At installation, Solus comes with a basic set of applications that are common to most Linux distributions, including [Firefox](https://www.mozilla.org/firefox/) and the [LibreOffice](https://en.wikipedia.org/wiki/LibreOffice) suite. To be honest, I don't make much use of LibreOffice in my work except for the spreadsheet program Calc. 

### Blog Writing

For my professional blog writing, I use a dedicated application called [Ghostwriter](https://wereturtle.github.io/ghostwriter/), which I first discovered through Solus several years ago. Ghostwriter is a [Markdown](https://daringfireball.net/projects/markdown/) editor, which is especially handy for writing projects destined for the web. 

### Web Browsing

Although Firefox generally meets my browsing needs, I find it is not always the best choice for interacting with Google-based applications. Many of my clients work through Google Docs, so I find it essential to use a second browser that provides better integration. 

Chrome is not direcly available for Solus, as it as a proprietary (closed-source) application. Solus also does not include the open-source [Chromim](https://www.chromium.org/Home) browser in its software repository. The Solus developers do, however, publish a script on their [third-party repository](https://getsol.us/articles/software/third-party/en/) that essentially downloads and converts Google's build of Chrome for Debian-based systems into a Solus package. 

The main limitation to this approach is that Chrome is not automatically updated each time Google releases a new version. The user needs to remember to run the third-party script again. Rather than deal with this, I decided to try [Vivaldi](https://vivaldi.com/), an open-source Chromium-based browser that is available in the Solus repository. So far, it's worked out great in terms of allowing me to work with Google Docs.

### Accounting

Although I've moved between various Linux distributions over the years, I've largely stuck with the same accounting software, a command-line utility called [hledger](https://hledger.org/). My loyalty to hledger has proven inconvenient at times since it is difficult to get working in Solus. Indeed, one reason I moved away from Solus for a time in favor of Ubnutu MATE was the latter supported hledger without any problems.

I admit I don't completely understand all the technical issues, but basically it comes down to this: hledger is written in the [Haskell](https://www.haskell.org/) programming language. The Solus implementation of Haskell is not up-to-date enough to support the more recent versions of hledger. I've spoken with Bryan Meyers, one of the Solus lead developers and the maintainer of the Haskell packages, and he's explained to me that the whole thing basically needs to be redone from scratch.

In the meantime, I've been able to use hledger thanks to the efforts of one of my fellow users, who [built a flatpak](https://groups.google.com/forum/#!topic/hledger/ZeTVmyerdhg) of one of the more recent versions. [Flatpak](https://flatpak.org/) is a distribution-independent packaging system supported by Solus. Although Flatpak is not commonly used for command-line tools like hledger, so far it has worked flawlessly on my Solus installation.

### Scheduling & Website Management

I've used a few different applications to keep track of my weekly writing schedule. Two that I've had great success with, and which are currently available in Solus,are [RedNotebook](https://rednotebook.sourceforge.io/) and [Zim](https://zim-wiki.org/). But for the moment, I'm managing my schedule using Markdown files in [Visual Studio Code](https://code.visualstudio.com/). Solus packages the open-source version of Visual Studio Code under the name ``vscode``.

Visual Studio Code has a built-in file explorer that displays all of the files in a given directory. I find this especially useful for handling my schedule, which I keep in a folder containing individual Markdown files for each week. Visual Studio Code also supports Markdown highlighting through an extension.

I also rely on Visual Studio Code to help manage the files for this website, [skipoliva.com](https://skipoliva.com). I use another package, [Hugo](https://gohugo.io/), to actually generate the website. I believe Hugo is also what the Solus developers use for their own project website.

## My Current Hardware

In 2019, I purchased a refurbished Dell Latitute E7240 directly from [Dell's website](https://www.dellrefurbished.com/). I paid about $200 for the laptop, which was originally manuafctured around 2013. The refurbished model I purchased came with a new 250 GB solid state drive and 8 GB of memory, which is more than sufficient to install and run Solus. The processor is an [Intel i5-4310U](https://ark.intel.com/content/www/us/en/ark/products/80343/intel-core-i5-4310u-processor-3m-cache-up-to-3-00-ghz.html) with four cores running at around 2 GHz apiece. 

While it's a nice work laptop, don't expect much in terms of gaming. The 12.5-inch display only has a resolution of 1366x768 pixels. I believe the graphics card is an integrated [Intel HD 4400](https://www.notebookcheck.net/Intel-HD-Graphics-4400.91979.0.html) processor. I manage to run [Stardew Valley](https://www.stardewvalley.net/) on it just fine, but again, I would not run higher-spec games on this machine.

## Why I Plan to Continue Using Solus in 2020

After using a dozen or so Linux distributions since 2012, I find Solus is still the best choice for getting work done on a (relatively) modern laptop. There are some limitations in terms of available software, as I discussed above, but the overall design and implementation of the operating system more than make up for any minor issues getting certain applications to work.

I would also cite the following factors in my decision to continue using Solus in 2020:

+ I like the developers. This isn't a technical reason for using Solus, mind you, but as a freelancer myself I greatly appreciate the time and dedication of the people who make Solus happen, particularly [lead developers Joshua Strobl and Bryan Meyers](https://getsol.us/solus/team/). I've had a chance to interact with them online and think they are good guys whose work should receive more public attention.
+ I appreciate the willingness of the developers to say "no." Again, this might sound like an odd reason for using Solus, but within the world of Linux distributions, this is a critical factor in maintaining a successful project. If you look at any Solus forum online, you will see individuals requesting support for additional desktop environments or demanding special accommodations for non-standard use cases. The developers have made it clear that Solus is not designed to be all things to all users. It has a specific, desktop-focused mission, and I appreciate that.
+ At the same time, because the Solus developers control their entire software stack, from the tools they use to build the operating system to the package management, they have a great deal of flexibility in making changes as circumstance warrant. I think this puts the developers in a better position to keep Solus relevant as we move forward into the new decade.
