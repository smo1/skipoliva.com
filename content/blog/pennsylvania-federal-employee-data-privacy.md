---
title: "Pa. State Court Ruling May Affect Federal Employee’s Data Privacy Rights"
date: "2019-03-30"
author: "S.M. Oliva"
tags:
  - "Pennsylvania"
categories:
  - Data Breach Litigation
---

Although data breaches are often discussed in the context of the impact on consumers, it is equally important to consider the employer-employee relationship. After all, many employers gather and store a significant amount of data regarding their employees and independent contractors. So what legal obligations, if any, must an employer meet when handling confidential or otherwise sensitive employee information?

## *Spade v. United States*: Prison Guard Sues Over Release of Personnel Files to Inmates

The Philadelphia-based U.S. Third Circuit Court of Appeals recently addressed a case that raises further complications by looking at the potential application of state and federal data privacy laws to a public employee. In this case, [*Spade v. United States*](https://scholar.google.com/scholar_case?case=10363288752664575925), the plaintiff was a correctional officer at a prison owned by the federal government. According to the plaintiff’s lawsuit, the government improperly provided an unredacted copy of his personnel file to an inmate who filed a Freedom of Information Act (FOIA) request. FOIA exempts from public release any “information that, if disclosed, would invade another individual’s personal privacy.” This typically includes personnel and medical records of federal employees.

### The Dittman Ruling

While the plaintiff’s appeal in *Spade* was pending, the Pennsylvania Supreme Court issued a decision in an unrelated case, [*Dittman v. UPMC*](https://scholar.google.com/scholar_case?case=15417377227266453038), in which a group of employees at a state-owned hospital filed suit after a data breach exposed the “names, birth dates, social security numbers, addresses, tax forms, and bank account information of all 62,000 [hospital] employees and former employees.”

Similar to the plaintiff in *Spade*, the *Dittman* employees asserted the hospital’s failure to prevent the data breach constituted negligence under Pennsylvania law. The Supreme Court agreed with the employees that an employer has a common-law duty in Pennsylvania “to exercise reasonable care in collecting and storing their personal and financial information on its computer systems.” Furthermore, this duty “exists independently from any contractual obligations between the parties.” This is important, because it means the employees do not need to prove they sustained any direct “economic loss” as a result of the data breach; they only need to show the employer was negligent in handling their personal information.

### Does *Dittman* Apply? Or Is This a Workers’ Compensation Case?

Going back to the *Spade* lawsuit, the Third Circuit asked the plaintiff and the federal government to address the potential impact of *Dittman*. In response, the government asked the appeals court to remand (or return) the case to the trial judge to deal with a separate issue–the potential application of another federal statute, the Federal Employees’ Compensation Act.

FECA is the federal equivalent of workers’ compensation law. It is designed to provide an “exclusive remedy” for any federal employee who sustains a “personal injury” while performing any work-related duties. Here, the government pointed to some prior decisions by the U.S. Department of Labor that suggested “releasing an employee’s [] personnel file to a nongovernmental organization” could be considered a personal injury covered by FECA.

Given all this, the Third Circuit said the Department of Labor would have to make a determination as to whether or not the plaintiff’s alleged injuries are covered by FECA. If so, that preempts his FTCA lawsuit. But if the Department ultimately decides FECA does not apply, than the trial judge will have to reconsider his earlier decision to dismiss the case in light of the Pennsylvania Supreme Court’s *Dittman* ruling.

