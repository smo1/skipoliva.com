---
author: "S.M. Oliva"
date: 2019-01-01
categories: "FOSS"
tags: [Markdown, Ghostwriter, Zim, LibreOffice, hledger]
title: Is FOSS Right for Your Freelance Business? 3 Questions to Ask as Part of Your Internal Software Audit
---
Before you consider switching to Linux from another operating system, it is critical to stop and think about the programs you actually use on a day-to-day basis. Many popular Windows and macOS business applications, such as Office 365 and Adobe's Creative Cloud, are not available natively on Linux. (It is technically possible to use these programs on Linux with the aide of special software, but this is not something I would recommend in most cases.) That said, there are typically FOSS applications that perform many--if not most--of the same functions as their proprietary counterparts. 

But even before you take a look at replacing your existing software library with FOSS, it is a good idea to conduct what I'll call an *internal software audit*. Basically, ask yourself the following questions with respect to your current software usage and ongoing needs:

## 1. Do You Have More Software Than You Need?

I started my career as a paralegal, which required heavy usage of Microsoft Office. Later, when I got into Linux and FOSS I moved, fairly seamlessly, to the [LibreOffice](https://www.libreoffice.org/) suite. But as my business transitioned from paralegal work to [blog ghostwriting services](https://skipoliva.com), I found the office suite was unnecessary. After all, you don't need a full-scale word processor to write blog posts.

Around this time, I learned about [Markdown](https://daringfireball.net/projects/markdown/), a tool developed by John Gruber that allows you to write human-readable documents in plain text and easily export them into HTML. Today, I do all of my blog writing in Markdown using a simple FOSS application called [Ghostwriter](https://wereturtle.github.io/ghostwriter/). Ghostwriter is a pared-down text editor that offers a number of features that are helpful to my work, including a running word count at the bottom of the screen, dark themes (i.e., white text on a black background), and auto-saving documents as I write them.

## 2. Is Your Data Easily Accessible?

Using Markdown also led me to recognize the importance of keeping my documents in formats that do not depend on one specific program to read. For example, I use [hledger](http://hledger.org/), a plain-text accounting tool, to do my bookkeeping. Hledger literally stores the entire accounting journal, and any reports I create, in a human-readable text file. So I don't even need hledger installed on a given computer to access my records.

Similarly, I use a FOSS application called [Zim](http://zim-wiki.org/) to manage my actual writing business. Zim is a tool to create wiki pages on your local computer. (It is similar in function to Evernote and other note-taking programs.) Like Markdown, all data in Zim is stored in plain-text files that contain special markup to indicate things like bold text or numbered lists. This means I can open and quickly glance at my writing schedule with any text editor, even on my phone. No special software is required to read or edit the file.

Obviously, plain-text files are not going to work for every situation. If you have more complex accounting needs, for instance, you probably require a program that can read QIF files, which are commonly used by banks to provide account information. Fortunately, there are FOSS applications that can read such formats. That is why it is important to review not just the software you use, but also the file formats your data relies upon.

## 3. How Do You Interact with Other Users?

When I worked as a contract paralegal, one of the main hurdles in using FOSS applications was cross-platform compatibility with Windows-based applications, particularly Microsoft Office. A key part of my job was editing Word documents. And while LibreOffice actually does a good job of opening and reading Word documents, there were always a few glitches when I would return a file to the attorney. Oftentimes, subtle formatting such as headers or footers would not appear the same in Word as in LibreOffice. It was not an insurmountable problem, but it was definitely a consideration. 

On the other hand, in my current freelance writing work I typically send finished blog posts to clients in a Google Docs file. Since Google Docs is a web-based application, it simply doesn't matter what operating system my personal computer uses. But if your own line of work requires you to frequently collaborate with others who rely on a specific piece of proprietary offline software, switching to a FOSS alternative might be impractical at the present time.

## No Linux Required to Start Working with FOSS Applications

One thing to keep in mind is that Linux is a type of operating system, not a programming language. In other words, many FOSS applications that are commonly associated with Linux are platform agnostic and work just fine on other operating systems. In fact, the developers of all of the FOSS programs mentioned in this post--[Ghostwriter](https://github.com/michelolvera/vs-ghostwriter), [hledger](http://hledger.org/download.html), [Zim](https://zim.glump.net/windows/), and [LibreOffice](https://www.libreoffice.org/download/download/)--make Windows-ready installations available. (It should be possible to install these programs on macOS as well, although you may need to do some extra work.) So even if you are not ready to consider dumping Windows for Linux, you can still incorporate a wide variety of FOSS applications into your business workflow.