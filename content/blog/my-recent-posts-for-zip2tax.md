---
author: "S.M. Oliva"
date: 2019-09-13
title: My Recent Posts for Zip2Tax
tags: [Zip2Tax, sales tax]
categories:
    - Sales Tax
---

Earlier this year, I renewed my relationship with [Zip2Tax](https://www.zip2tax.com/), a leading provider of sales tax database information. I contribute periodic articles for the company’s blog discussing recent legal developments affecting state sales and use tax laws. Here are links to my recent posts:

* [Illinois Will Require Online Marketplaces to Collect Sales Taxes for Out-of-State Sellers Starting in 2020](https://blog.zip2tax.com/leveling-the-playing-field-for-illinois-retail-act/)
* [Are College Meal Plans Taxable or Not?](https://blog.zip2tax.com/are-college-meal-plans-taxable-or-not/)
* [Report: Canadian Government Fails to Collect $169 Million in Sales Taxes Annually on Digital Goods & Services](https://blog.zip2tax.com/canada_digital_goods_tax/)
* [Maine Legislators Consider Authorizing Local-Option Sales Taxes](https://blog.zip2tax.com/maine-legislators-consider-lost/)
* [Georgia House to Require “Marketplace Facilitators” like Uber, Airbnb, and Ebay to Collect Sales Taxes](https://blog.zip2tax.com/georgia-house-goes-a-step-further-to-collect-sales-taxes/)
* [Missouri Supreme Court Says Operator of Federal Reserve Cafeteria Must Collect Sales Tax](https://blog.zip2tax.com/employer-sponsored-cafeteria/)